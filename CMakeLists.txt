cmake_minimum_required(VERSION 3.10.0)
project(chatter VERSION 0.1.0)

include(CTest)
include(CPack)

#custom modules
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")

include(chatter_portable)

# CTEST
enable_testing()

# Chatter-Net target
set(SOURCES "empty")
get_chatter_net_src(SOURCES)
add_library(chatter_net STATIC ${SOURCES})
target_include_directories(chatter_net PUBLIC
                            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

# Chatter CLI Option handling target
set(CLI_SOURCES src/cli_if.c)
add_library(chatter_cli_opt STATIC ${CLI_SOURCES})
target_include_directories(chatter_cli_opt PUBLIC
                            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

# main Chatter target
add_executable(chatter_main src/main.c)
target_include_directories(chatter_main PUBLIC
                            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

target_link_libraries(chatter_main PUBLIC chatter_net)
target_link_libraries(chatter_main PUBLIC chatter_cli_opt)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
