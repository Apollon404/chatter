#include <daemon.h>
#include <chatter.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>

/*
     --------- system-specific implementations -----------------------
*/

struct srv_net_handle
{
    int srv_fd;
    int conn_fd;
    struct sockaddr_in srv;
    struct sockaddr_in client;
    unsigned int length;
};

/*
    ---------- portable functions ---------
*/
struct srv_net_handle *
srv_net_init()
{
    struct srv_net_handle * net_h = malloc(sizeof(struct srv_net_handle));
    net_h->srv_fd = 0;
    net_h->conn_fd = 0;

    net_h->srv_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(net_h->srv_fd < 0)
    {
        fputs("could not create socket", stdout);
        return NULL;
    }

    memset(&(net_h->srv), 0, sizeof(net_h->srv));
    net_h->srv.sin_family = AF_INET;
    net_h->srv.sin_addr.s_addr = htonl(INADDR_ANY);

    net_h->srv.sin_port = htons(CHATTER_PORT);

    if(bind(net_h->srv_fd, (struct sockaddr*)&(net_h->srv),
            sizeof(net_h->srv)) < 0)
    {
        fputs("could not bind socket", stdout);
    }

    if(listen(net_h->srv_fd, 5) == -1)
    {
        fputs("could not listen", stdout);
    }

    return net_h;
}


/*

*/
int
_srv_single_chat_loop()
{

  return 0;
}


int
srv_chat_loop(struct srv_net_handle *net_h)
{
    if(net_h == NULL)
    {
        return 1;
    }

    for(;;)
    {
        net_h->length = sizeof(net_h->client);
        net_h->conn_fd = accept(net_h->srv_fd,
                                (struct sockaddr*)&(net_h->client), &net_h->length);
        if(net_h->conn_fd < 0)
        {
            fputs("accept failed", stdout);
        }
        fprintf(stderr, "Client connected: %s\n",
                inet_ntoa(net_h->client.sin_addr));
    }

    return srv_kill_net(net_h);
}


int
srv_kill_net(struct srv_net_handle *net_h)
{
    close(net_h->srv_fd);
    close(net_h->conn_fd);
    free(net_h);
}
