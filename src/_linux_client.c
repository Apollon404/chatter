#include <client.h>
#include <chatter.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

// -------------- system specific Definitions ---------------------
struct cl_net_handle
{
    int sock_fd;
    int count;
    struct sockaddr_in serv_addr;
};


//char recvBuf[MAX_MSG_LENGTH];


// ----------------portable functions -------------------
struct cl_net_handle *
cl_net_init()
{
    struct cl_net_handle *net_h = malloc(sizeof(struct cl_net_handle));
    net_h->sock_fd = 0;
    net_h->count = 0;

    net_h->sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(net_h->sock_fd < 0)
    {
      fputs("could not create socket", stdout);
      return NULL;
    }

    return net_h;
}


int
cl_connectto(struct cl_net_handle *net_h, s_data sdata)
{
    memset(&(net_h->serv_addr), 0, sizeof(net_h->serv_addr));

    if(sdata.addr != INADDR_NONE)
    {
        memcpy((char *)&(net_h->serv_addr.sin_addr),
                &(sdata.addr), sizeof((sdata.addr)));
    }

    net_h->serv_addr.sin_family = AF_INET;

    net_h->serv_addr.sin_port = htons(sdata.port);

    if(connect(net_h->sock_fd, (struct sockaddr*)&(net_h->serv_addr),
                sizeof(net_h->serv_addr)) < 0)
    {
        fputs("could not connect to server\n", stderr);
        return 1;
    }

    return 0;
}


int
cl_chat_loop(struct cl_net_handle *net_h)
{
    // TODO: Schicken und Senden von Nachirchten

   return cl_net_kill(net_h);
}


int
cl_net_kill(struct cl_net_handle *net_h)
{
    close(net_h->sock_fd);
    free(net_h);

    return 0;
}

void
cl_sdata_init(s_data *srv, const char* c)
{
    *srv = (s_data) { inet_addr(c),
            CHATTER_PORT};
}
