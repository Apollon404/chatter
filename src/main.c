#include <chatter.h>
#include <client.h>
#include <daemon.h>
#include <cli_if.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>


int
main(int argc, char **argv)
{
    struct cli_opt_set *opt_s = cli_opt_set_init();

    struct cl_net_handle *cl_net;
    struct srv_net_handle *srv_net;

    s_data srv;

    cli_add_option(opt_s, 's', NULL, "servermode");
    cli_add_option(opt_s, 'h', NULL, "host_addr");
    cli_add_option(opt_s, 'p', "port", "port");

    cli_opt_exec(opt_s, argc, argv);

    if(cli_is_enabled(opt_s, "servermode"))
    {
        if((srv_net = srv_net_init()) == NULL)
        {
            fputs("Server Init failed", stderr);
        }

        return srv_chat_loop(srv_net);
    }
    else
    {
        if(cli_is_enabled(opt_s, "host_addr"))
        {
            cl_sdata_init(&srv, cli_retrieve_option(opt_s, "host_addr")[0]);

            fputs("Welcome to Chatter!\n", stdout);

            if((cl_net = cl_net_init()) == NULL)
            {
                // TODO: errorhandling
            }

            //attempt to connect to server
            if(cl_connectto(cl_net, srv) != 0)
            {
                fprintf(stdout, "Could not connect to Server on %s\n", 
                        cli_retrieve_option(opt_s, "host_addr")[0]);
                return EXIT_FAILURE;
            }
            else
            {
                fprintf(stdout, "Successfully connected to Server: %s\n", 
                        cli_retrieve_option(opt_s, "host_addr")[0]);
            }
            // "main" client communication loop


            return cl_chat_loop(cl_net);
        }
        else
        {
            fputs("please specifiy a address to connect to in clientmode with the -h option\n", stderr);
            return EXIT_FAILURE;
        }
    }

}
