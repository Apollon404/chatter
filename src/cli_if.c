#include <cli_if.h>
#include <string.h>
#include <stdlib.h>

#define VALUE_MAX 64

static struct _cli_option *
_cli_get_option_by_key(struct cli_opt_set *oset, const char* key)
{
    for(int i = 0; i <= oset->count; ++i)
    {
        if(!(strcmp(oset->opts[i]->key, key)))
        {
            return oset->opts[i];
        }
    }

    return NULL;
}



static struct _cli_option *
_cli_get_option_by_sindex(struct cli_opt_set *oset, const char sindex)
{
    for(int i = 0; i <= oset->count; ++i)
    {
        if(oset->opts[i]->short_index[0] == sindex)
        {
            return oset->opts[i];
        }
    }
}



static void
_cli_opt_free(struct _cli_option * opt)
{
    for(int i = 0; i < opt->valc; ++i)
    {
        free(opt->values[i]);
    }

    free(opt);
}


struct cli_opt_set*
cli_opt_set_init()
{
    struct cli_opt_set* ret = malloc(sizeof(struct cli_opt_set));
    *ret = (struct cli_opt_set) {0, NULL};
    return ret;
}


int
cli_add_option(struct cli_opt_set *set, const char s_index, const char *v_index,
                const char *key)
{
    set->opts[set->count] = malloc(sizeof(struct _cli_option));
    if(s_index)
    {
        strcpy(set->opts[set->count]->key, key);
        set->opts[set->count]->short_index[0] = s_index;
        set->opts[set->count]->valc = 0;
    }
    else
    {
        return 1;
    }
    
    // TODO: add verbose option handling
    
    ++(set->count);
}


bool
cli_is_enabled(struct cli_opt_set *oset, const char* key)
{
    return _cli_get_option_by_key(oset, key)->valc;
}


char**
cli_retrieve_option(struct cli_opt_set *oset, const char *key)
{
    return _cli_get_option_by_key(oset, key)->values;
}


void
cli_opt_set_free(struct cli_opt_set *oset)
{
     for(int i = 0; i <= oset->count; ++i)
    {
        _cli_opt_free(oset->opts[i]);
    }
    free(oset);
}


int
cli_opt_exec(struct cli_opt_set *set, int argc, char **argv)
{
    struct _cli_option *it;

    for(int i = 0; i < argc; ++i)
    {
        if(argv[i][0] == '-')
        {
            if((it = _cli_get_option_by_sindex(set, argv[i][1])) != NULL)
            {
                for(++i; i < argc; ++i)
                {
                    if(argv[i][0] == '-')
                    {
                        --i;
                        break;
                    }
                    else
                    {
                        it->values[it->valc] = malloc(VALUE_MAX);
                        strcpy(it->values[it->valc], argv[i]);
                        ++(it->valc);
                    }
                }
                if(it->valc == 0)
                {
                    it->valc = -1;
                }
            }
        }
    }
}
