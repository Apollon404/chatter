function(get_chatter_net_src SOURCES)
    if(WIN32)
        file(GLOB src src/_win* )
        set(SOURCES ${src} PARENT_SCOPE)
    endif(WIN32)
    
    if(UNIX)
        file(GLOB src src/_linux* )
        set(SOURCES ${src} PARENT_SCOPE)
    endif(UNIX)

endfunction(get_chatter_net_src Sources)
