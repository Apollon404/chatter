#ifndef CLIIF_H
#define CLIIF_H

#include <stddef.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#define SHORT_INDEX_MAX 1
#define VERBOSE_INDEX_MAX 32
#define KEY_MAX 128

struct _cli_option
{
  char key[KEY_MAX];
  char short_index[SHORT_INDEX_MAX];
  char verb_index[VERBOSE_INDEX_MAX];
  
  int valc;
  char *values[10];
};


struct cli_opt_set
{
  size_t count;
  struct _cli_option *opts[10];
};


struct cli_opt_set*
cli_opt_set_init();

int
cli_add_option(struct cli_opt_set *set, const char s_index, const char *v_index,
                const char *key);


int
cli_opt_exec(struct cli_opt_set *set, int argc, char **argv);


char**
cli_retrieve_option(struct cli_opt_set *oset, const char *key);


bool
cli_is_enabled(struct cli_opt_set *oset, const char* key);

void
cli_opt_set_free(struct cli_opt_set *oset);

#endif
