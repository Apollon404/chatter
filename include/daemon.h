/*


  Author: Paul Jena
*/
#ifndef DAEMON_H
#define DAEMON_H

#include <chatter.h>
#include <client.h>

struct Host
{
    char name[MAX_NAME_LENGTH];
    s_data addr;
    struct Host *next;
};
typedef struct Host Host;

struct srv_net_handle;

/*
----------- portable daemon functions to enable the servermode ------------
*/
struct srv_net_handle *
srv_net_init();

int
srv_chat_loop(struct srv_net_handle *net_h);

int
srv_kill_net(struct srv_net_handle *net_h);

#endif
