/*


    Author: Paul Jena
*/
#ifndef CHATTER_H
#define CHATTER_H

//-------------- Chatter - Global Variables
#define MAX_NAME_LENGTH  16
#define MAX_MSG_LENGTH  64
#define CHATTER_PORT  6690

// ---------------- Chatter-global typedefs -------------------------------
typedef unsigned char cherr_code;

#endif
