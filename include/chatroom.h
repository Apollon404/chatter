/*

    Author: Paul Jena
*/
#ifndef CHATROOM_H
#define CHATROOM_H

#include <sys/types.h>
#include <time.h>

#include <chatter.h>

struct Message
{
    char str[MAX_MSG_LENGTH];
    time_t timestamp;
    struct Message *next;
};
typedef struct Message Message;


/*
    Gibt den Chatverlauf dieses Prozesses in eine Textdatei aus. Hauptsaechlich fuer
    Logging gedacht.
*/
int
chat_to_file(Message* first);

#endif
