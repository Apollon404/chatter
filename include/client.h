/*

    Author: Paul Jena
*/
#ifndef CLIENT_H
#define CLIENT_H

#include <chatter.h>

struct server_data
{
    unsigned long addr;
    unsigned int port;
};
typedef struct server_data s_data;

struct cl_net_handle;

// ------ portable network functions with system specific implementation -------
void
cl_sdata_init(s_data *, const char*);

struct cl_net_handle *
cl_net_init();

// connects to s_data
int
cl_connectto(struct cl_net_handle *net_h, s_data);


int
cl_chat_loop(struct cl_net_handle *net_h);

int
cl_net_kill(struct cl_net_handle *net_h); // frees resources allocated in net_init()

#endif
